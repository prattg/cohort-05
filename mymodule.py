# This is a module
# It lives in the file mymodule.py

def dummy():
    return 45

def foo():
    print('bar!')
    return 1

import sys
import operator

# Dictionary of operators
ops = {
    "+" : operator.add,
    "-" : operator.sub,
    "*" : operator.mul,
    "%" : operator.mod,
    "/" : operator.truediv,
    "//": operator.floordiv,
    "^" : operator.pow,
}

def dict_calc(x, y, operator, op_dict=ops):

   # Update needed here as we are note taking arguments from command line
    # if len(args) != 4:
    #     print('Please provide THREE parameters" 2 numbers and an operator.')
    #     sys.exit()
    #     print(f'Script Name is {sys.argv[0]}')
    # print(f'Expression to evalulate is {sys.argv[1]} {sys.argv[3]} {sys.argv[2]}')
    
    #Data validation: Make sure the first 2 parameters are numbers.  If so, set to variables.
    try:
        num1 = float(sys.argv[1])
        num2 = float(sys.argv[2])
    except ValueError:
        print("The first 2 parameters must be numbers!!  Try again.\n")
        sys.exit()

    op = sys.argv[3]
    #Data validation: Make sure the 3rd parameter is an operator and in the dictonary!
    if op in ops:
        pass
    else:
        print("Your 3rd parameter is not a valid operand (+ - * ? % ^).  Try again.\n")
        sys.exit()

    # def dict_calc(x, y, operator, op_dict=ops):
    operation = op_dict.get(operator)
    if operation:
        try:
            answer = operation(x , y)
            return answer
            # print("The answer is", dict_calc(num1, num2, op),"\n")

        except ZeroDivisionError:
            print("You cannot divide by zero!  Try again.\n")
            return None

public_data = "public stuff!"
# names that begin with _ are considered "private"
_private_data = "private stuff!"
