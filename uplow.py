#!/usr/bin/env python3
strstring = input("Enter a string: ")
intstride = int(input("Enter a \"stride\" (integer): "))
i = 0
strct = 1
strsub = strstring[i:i+intstride]
newstring = ""
while strsub:
    if strct % 2 != 0:
        newstring += strsub.upper()
    else:
        newstring += strsub.lower()
    i = i + intstride
    strct = strct + 1
    strsub = strstring[i:i+intstride]
print(newstring)
